﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PastelDeFlango.Models.Migrations
{
    public partial class backendv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Avatar_Level_LevelId",
                table: "Avatar");

            migrationBuilder.DropTable(
                name: "AvaDesafio");

            migrationBuilder.DropTable(
                name: "AvatarHasRecompensa");

            migrationBuilder.DropTable(
                name: "Desafio");

            migrationBuilder.DropTable(
                name: "Status");

            migrationBuilder.DropTable(
                name: "Recompensa");

            migrationBuilder.DropTable(
                name: "Level");

            migrationBuilder.DropIndex(
                name: "IX_Avatar_LevelId",
                table: "Avatar");

            migrationBuilder.DropColumn(
                name: "LevelId",
                table: "Avatar");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LevelId",
                table: "Avatar",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Desafio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Desafio = table.Column<string>(nullable: true),
                    Descricao = table.Column<string>(nullable: true),
                    Dificuldade = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Desafio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Level",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LevelIndice = table.Column<int>(nullable: false),
                    QdtXpProxLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Level", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Status",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StatusName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Status", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Recompensa",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descricao = table.Column<string>(nullable: true),
                    LevelMaximoId = table.Column<int>(nullable: true),
                    LevelMinimoId = table.Column<int>(nullable: true),
                    NomeRecompensa = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recompensa", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Recompensa_Level_LevelMaximoId",
                        column: x => x.LevelMaximoId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Recompensa_Level_LevelMinimoId",
                        column: x => x.LevelMinimoId,
                        principalTable: "Level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AvaDesafio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AvatarId = table.Column<int>(nullable: true),
                    DesafioId = table.Column<int>(nullable: true),
                    StatusId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AvaDesafio", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AvaDesafio_Avatar_AvatarId",
                        column: x => x.AvatarId,
                        principalTable: "Avatar",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AvaDesafio_Desafio_DesafioId",
                        column: x => x.DesafioId,
                        principalTable: "Desafio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AvaDesafio_Status_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Status",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AvatarHasRecompensa",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AvatarId = table.Column<int>(nullable: true),
                    RecompensaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AvatarHasRecompensa", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AvatarHasRecompensa_Avatar_AvatarId",
                        column: x => x.AvatarId,
                        principalTable: "Avatar",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AvatarHasRecompensa_Recompensa_RecompensaId",
                        column: x => x.RecompensaId,
                        principalTable: "Recompensa",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Avatar_LevelId",
                table: "Avatar",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_AvaDesafio_AvatarId",
                table: "AvaDesafio",
                column: "AvatarId");

            migrationBuilder.CreateIndex(
                name: "IX_AvaDesafio_DesafioId",
                table: "AvaDesafio",
                column: "DesafioId");

            migrationBuilder.CreateIndex(
                name: "IX_AvaDesafio_StatusId",
                table: "AvaDesafio",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_AvatarHasRecompensa_AvatarId",
                table: "AvatarHasRecompensa",
                column: "AvatarId");

            migrationBuilder.CreateIndex(
                name: "IX_AvatarHasRecompensa_RecompensaId",
                table: "AvatarHasRecompensa",
                column: "RecompensaId");

            migrationBuilder.CreateIndex(
                name: "IX_Recompensa_LevelMaximoId",
                table: "Recompensa",
                column: "LevelMaximoId");

            migrationBuilder.CreateIndex(
                name: "IX_Recompensa_LevelMinimoId",
                table: "Recompensa",
                column: "LevelMinimoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Avatar_Level_LevelId",
                table: "Avatar",
                column: "LevelId",
                principalTable: "Level",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
