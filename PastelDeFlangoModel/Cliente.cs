﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace PastelDeFlango.Models
{
    public class Cliente: IdentityUser
    {

        [PersonalData]
        public Curso Curso { get; set; }
        [PersonalData]
        public Sexo Sexo { get; set; }
        [PersonalData]
        public Avatar Avatar { get; set; }
        [PersonalData]
        public int AvatarId { get; set; }

    }
}
