﻿using System;

namespace PastelDeFlango.Models
{
    public class Avatar
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public int Xp { get; set; }
        public int TempoJogo { get; set; }
        public Guid ImagemId { get; set; }
    }
}