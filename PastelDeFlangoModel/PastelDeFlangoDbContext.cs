﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace PastelDeFlango.Models
{
    public class PastelDeFlangoDbContext : IdentityDbContext<Cliente>
    {
        public PastelDeFlangoDbContext(DbContextOptions<PastelDeFlangoDbContext> options)
            : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer("Server=pfdbinstance.csg5fa6lsbud.us-east-1.rds.amazonaws.com;Database=PastelDeFlango;User Id=pasteldeflango;Password=pasteldeflango; ");
        }
        public DbSet<Avatar> Avatar { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Curso> Curso{ get; set; } 
        public DbSet<Sexo> Sexo { get; set; }

    }
}


