﻿namespace PastelDeFlango.Models
{
    public class Curso
    {
        public int Id { get; set; }
        public string Matricula { get; set; }
        public string NomeCurso { get; set; }

    }
}