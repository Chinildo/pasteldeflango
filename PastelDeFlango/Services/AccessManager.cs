﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using PastelDeFlango.Models;
using System.Threading.Tasks;
using System.Linq;

namespace PastelDeFlango
{
    public class AccessManager : IAccessManager
    {
        private UserManager<Cliente> _userManager;
        private SignInManager<Cliente> _signInManager;
        private readonly PastelDeFlangoDbContext _context;
        

        //private SigningConfigurations _signingConfigurations;
        //private TokenConfigurations _tokenConfigurations;

        public AccessManager(
            UserManager<Cliente> userManager,
            SignInManager<Cliente> signInManager,
            PastelDeFlangoDbContext context
            )
        //SigningConfigurations signingConfigurations,
        //TokenConfigurations tokenConfigurations)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            //_signingConfigurations = signingConfigurations;
            //_tokenConfigurations = tokenConfigurations;
        }

        public async Task<object> ValidateCredentials(string username, string password)
        {
            
            bool succeeded = false;
            if (username != null && !String.IsNullOrWhiteSpace(username))
            {
                // Verifica a existência do usuário nas tabelas do
                // ASP.NET Core Identity
                
                var usuario = _userManager
                    .FindByNameAsync(username).Result;

                if (usuario != null)
                {
                    // Efetua o login com base no Id do usuário e sua senha
                    var resultadoLogin = _signInManager 
                        .CheckPasswordSignInAsync(usuario, password, false)
                        .Result;
                    if (resultadoLogin.Succeeded)
                    {
                        usuario.Avatar = await _context.Avatar.FindAsync(usuario.AvatarId);
                        return new
                        {
                            usuario,
                            succeeded = true
                        };                        
                    }
                }
            }

            return new {
                succeeded
            } ;
        }

        public async Task<object> Registrar(Avatar avatar, Cliente usuario, string password)
        {
            var userIdentity = _userManager
                    .FindByNameAsync(usuario.UserName).Result;

            if (userIdentity != null)
            {
                return new
                {
                    Registered = false,
                    Message = "usuario já registrado"
                };
            }
            else
            {
                _context.Avatar.Add(avatar);
                await _context.SaveChangesAsync();
                usuario.AvatarId = avatar.Id;
                var resultado = _userManager
                   .CreateAsync(usuario, password).Result;

                return new
                {
                    usuario
                };
            }
        }


    }
}