﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PastelDeFlango.Models;

namespace PastelDeFlango.Controllers
{
    [Route("api/avatar")]
    [ApiController]
    public class AvatarsController : ControllerBase
    {
        private readonly PastelDeFlangoDbContext _context;

        public UserManager<Cliente> UserManager { get; }
        public SignInManager<Cliente> SignInManager { get; }
        //classe usermanager para criar login
        //signinmanager<Cliente>
        //classe de log 
        // transaction vou no contexto e inicia begintransanction
        public AvatarsController(PastelDeFlangoDbContext context)
        {
            _context = context;
        }

        // GET: api/Avatars
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Avatar>>> GetAvatar()
        //{
        //    return await _context.Avatar.ToListAsync();
        //}

        // GET: api/Avatars/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Avatar>> GetAvatar([FromBody]int id)
        {
            var avatar = await _context.Avatar.FindAsync(id);

            if (avatar == null)
            {
                return NotFound();
            }

            return avatar;
        }

        //[HttpPost("register")]
        //public async Task<ActionResult<Guid>> Register([FromBody]string login, string password)
        //{
        //    var user = UserManager.AddLoginAsync(new User

        //    //return user = null ?? user.Id;

        //    return Guid.NewGuid();
        //}

        //[HttpPost("signin")]
        //public async Task<ActionResult<Guid>> SignIn([FromBody]string login, string password)
        //{
        //    //var user = UserManager.FindByLoginAsync(login, password);

        //    //return user = null ?? user.Id;

        //    return Guid.NewGuid();
        //}


        // GET: api/ranking/
        [HttpGet("ranking/{id}")]
        public async Task<ActionResult<IList>> GetRanking(int id )
        {
            //nickname = "usuario6";
            var avatar = await _context.Avatar.FindAsync(id);
            //var avatar = await _context.Avatar.FindAsync("nickname", nickname);
            //var avatar = await _context.Avatar.FindAsync(nickname);
            //var avatarList = _context.Avatar.FromSql("SELECT * FROM[PastelDeFlango].[dbo].[Avatar] order by - Xp")
            //                          .ToList();
            var top3Avatar = _context.Avatar.FromSql($"SELECT TOP 3 *  FROM[PastelDeFlango].[dbo].[Avatar] where Xp > {avatar.Xp} order by - Xp")
                          .ToArray();

            var acimaAvatar3 = _context.Avatar.FromSql($"SELECT TOP 3 *  FROM[PastelDeFlango].[dbo].[Avatar] where Xp > {avatar.Xp} order by + Xp")
                          .ToArray();
            var abaixoAvatar3 = _context.Avatar.FromSql($"SELECT TOP 3 *  FROM[PastelDeFlango].[dbo].[Avatar] where Xp < {avatar.Xp} order by - Xp")
                         .ToArray();
            

            Console.WriteLine(avatar.ToString());
            List<Avatar> avatarList = new List<Avatar>();
            foreach (Avatar element in top3Avatar)
            {
                avatarList.Add(element);
            }
            foreach (Avatar element in acimaAvatar3.Reverse())
            {
                foreach (Avatar elementTop3 in top3Avatar)
                {
                    if(!element.Equals(elementTop3)){
                        avatarList.Add(element);
                    }
                }   
            }
            avatarList.Add(avatar);
            foreach (Avatar element in abaixoAvatar3)
            {
                avatarList.Add(element);
            }
            if (avatarList == null)
            {
                return NotFound();
            }

            if (avatarList.Count <= 10)
            {
                return avatarList;
            }

            //var listaReturn = new Avatar[10];
            //avatarList.ForEach(verificarRanking());
            //listaReturn.Append
            return avatarList;
        }


        // PUT: api/Avatars/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAvatar(int id, [FromBody]Avatar avatar)
        {
            if (id != avatar.Id)
            {
                return BadRequest();
            }

            _context.Entry(avatar).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AvatarExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Avatars
        [HttpPost]
        public async Task<ActionResult<Avatar>> PostAvatar(Avatar avatar)
        {
            _context.Avatar.Add(avatar);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAvatar", new { id = avatar.Id }, avatar);
        }



        // DELETE: api/Avatars/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Avatar>> DeleteAvatar(int id)
        {
            var avatar = await _context.Avatar.FindAsync(id);
            if (avatar == null)
            {
                return NotFound();
            }

            _context.Avatar.Remove(avatar);
            await _context.SaveChangesAsync();

            return avatar;
        }   
        

        private bool AvatarExists(int id)
        {
            return _context.Avatar.Any(e => e.Id == id);
        }
    }
}
