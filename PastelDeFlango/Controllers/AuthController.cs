﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using PastelDeFlango.Models;

namespace PastelDeFlango.Controllers
{
    [Route("api")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ApiController]
    public class VersionController : ControllerBase{
    [HttpGet("version")]
            public string Version(){
                return "API GAMECICLANDO VERSÃO 0.0.1";
            }

    }
    [Route("api/auth")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly PastelDeFlangoDbContext _context;

        public UserManager<Cliente> UserManager { get; }
        public SignInManager<Cliente> SignInManager { get; }

        public AuthController()
        {
            
        }

        // POST: api/auth/signIn
        [HttpPost("signin")]
        public async Task<object> Post([FromBody]JObject entrada, [FromServices] UserManager<Cliente> userManager, [FromServices] SignInManager<Cliente> signInManager, [FromServices] PastelDeFlangoDbContext context)
         {  
            AccessManager accessManager = new AccessManager(userManager, signInManager, context);

            string username;
            string password;
            try
            {
                username = entrada.GetValue("userName").ToString();
                password = entrada.GetValue("password").ToString();
                return await accessManager.ValidateCredentials(username, password);
            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return new
                {
                    Registered = false,
                    Message = "Erro ao efetuar login"
                };
            }
            
        }

        // POST: api/auth/register
        [HttpPost("register")]
        // nickname, login(email) senha
        public async Task<object> Register([FromBody] JObject entrada, [FromServices] UserManager<Cliente> userManager, [FromServices] SignInManager<Cliente> signInManager, [FromServices] PastelDeFlangoDbContext context)
        {

            AccessManager accessManager = new AccessManager(userManager, signInManager, context);

            Cliente usuario;
            Avatar avatar;
            string password;

            try
            {
                JToken jtk = entrada.GetValue("cliente");
                password = entrada.GetValue("password").ToString();
                usuario = jtk.ToObject<Cliente>();
                avatar = new Avatar();
                usuario.Avatar = avatar;
                usuario.Avatar.Nickname = usuario.UserName;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return new
                {
                    Registered = false,
                    Message = "Erro ao cadastrar"
                };
            }
            



            if (usuario == null)
            {
                return new
                {
                    Registered = false,
                    Message = "informe todos os campos"
                };

            }


            return await accessManager.Registrar(avatar, usuario, password);

        }

    }
}